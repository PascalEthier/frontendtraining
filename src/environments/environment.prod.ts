﻿export const environment = {
	production: true,

	// URL of production API
	apiUrl: 'http://scorecard-1805409954.us-west-2.elb.amazonaws.com',
	pageSize: 10
};
