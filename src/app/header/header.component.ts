﻿import { Component, OnInit } from '@angular/core';
import { Router, RoutesRecognized  } from '@angular/router';

@Component({
	selector: 'app-header',
   templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	showBack: boolean = false;

	constructor(private router: Router) {
      
	}

	private resolveShowBackState(newState: boolean) {
      this.showBack = newState;
	}

	ngOnInit() {
		//Subscribe to changes on the route
		this.router.events
			.subscribe(val => {
				if (val instanceof RoutesRecognized) {
					this.resolveShowBackState(val.state.root.firstChild.params['username'] != null);
				}
		});
	}

}
