﻿import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Headers, Http, RequestOptions } from '@angular/http';

import { Router } from '@angular/router';

import { LoginInfo } from './login-info';

import 'rxjs/add/operator/toPromise';

const API_URL = environment.apiUrl;


@Injectable()
export class LoginService {
   //Store the current state
	private currentLoginInfoState: LoginInfo = new LoginInfo(false);

	constructor(private http: Http, private router: Router) {
		//Retrieve the initial state of whether or not you are logged in
		let username: string = localStorage.getItem("user");
		if (username != null) {
			this.setLoggedIn(new LoginInfo(true, username));
			this.login(username);
		} else {
			this.setLoggedIn(new LoginInfo(false));
		}
	}

   private foundCookie(name) : boolean {
	   var value = "; " + document.cookie;
	   var parts = value.split("; " + name + "=");
		if (parts.length == 2)
			return true;

		return false;
	}

	get isAuthenticated(): boolean {
      //check for the presence of a cookie with nemae= ".AspNetCore.MyCookieMiddlewareInstance"
		return localStorage.getItem("user") != null;
	}

	private setLoggedIn(info: LoginInfo) {
		this.currentLoginInfoState = info;
		if (info.isLoggedIn) {
			localStorage.setItem("user", info.username);
		} else {
			localStorage.removeItem("user");
         this.router.navigate(['/']);
		}
      
	}

	login(username): Promise<any> {
		return this.http
			.post(API_URL + "/login?username=" + username, null, {withCredentials: true})
			.toPromise()
			.then(response => {
				this.setLoggedIn(new LoginInfo(true, username));
				return true;
			})
			.catch(this.handleError);
	}

	logout(): Promise<any> {
      //Making an assumption: We don't actually care whether the server has received and processed the request
		this.setLoggedIn(new LoginInfo(false));
		return this.http
			.get(API_URL + "/logout", { withCredentials: true })
			.toPromise()
			.then(response => () => {
				return true;
			})
			.catch(this.handleError);
	}

	getCurrentLoginInfo(): LoginInfo {
		return this.currentLoginInfoState;
	}

	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error); // for demo purposes only
		return Promise.reject(error.message || error);
	}

}
