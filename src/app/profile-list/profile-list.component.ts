﻿import { Component, OnInit, Input } from '@angular/core';
import { Profile } from 'app/profile';


@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})

export class ProfileListComponent{


    profiles: Profile[] =  [{ "name": "Uranus" },
    { "name": "HotToddy" },
    { "name": "Jackd" },
    { "name": "FeelX" }];
}
