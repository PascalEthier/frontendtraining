﻿import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RouterTestingModule } from '@angular/router/testing';

import { Profile } from '../profile';

import { ProfileListComponent } from './profile-list.component';

@Component({
    template: ''
})
class MockProfileScorecardsComponent { }

@NgModule({
    declarations: [MockProfileScorecardsComponent],
    exports: [MockProfileScorecardsComponent]
})
class MockModule { }


describe('ProfileListComponent', () => {
  let component: ProfileListComponent;
  let fixture: ComponentFixture<ProfileListComponent>;
  let expectedProfiles: Profile[];
  let de: DebugElement;
  let profileListEl: HTMLElement;

  beforeEach(() => {
      TestBed.configureTestingModule({
          declarations: [ProfileListComponent],
          imports: [MockModule, RouterTestingModule.withRoutes([
              { path: '', component: MockProfileScorecardsComponent },
              { path: 'profiles/:username/scorecards', component: MockProfileScorecardsComponent },
          ])]
      })
      .compileComponents();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileListComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('.profile-list'));

    expectedProfiles = [{ "name": "Uranus" },
    { "name": "HotToddy" },
    { "name": "Jackd" },
    { "name": "FeelX" }];

    component.profiles = expectedProfiles;
    fixture.detectChanges();
  });

  it('should display the proper number of names', () => {
      expect(de.queryAll(By.css('li')).length).toBe(4);
  })

  it('Should have proper url on link', () => {
      let href = fixture.debugElement.query(By.css('a')).nativeElement.getAttribute('href');//Get the first anchor link
      expect(href).toEqual('/profiles/Uranus/scorecards');
      
  });

});
