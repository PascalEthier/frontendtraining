﻿import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-approve-disaprove',
  templateUrl: './approve-disaprove.component.html',
  styleUrls: ['./approve-disaprove.component.scss']
})
export class ApproveDisaproveComponent implements OnInit {
	@Output() onApprove = new EventEmitter();
	@Output() onDisaprove = new EventEmitter();

   constructor() { }

	approve() {
		this.onApprove.emit();
		return false;
	}

	disaprove() {
		this.onDisaprove.emit();
		return false;
	}

   ngOnInit() {
   }

}
