import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveDisaproveComponent } from './approve-disaprove.component';

describe('ApproveDisaproveComponent', () => {
  let component: ApproveDisaproveComponent;
  let fixture: ComponentFixture<ApproveDisaproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveDisaproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveDisaproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
