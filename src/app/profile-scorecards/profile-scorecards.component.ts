﻿import { Component, OnInit, NgModule } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { Scorecard } from 'app/scorecard';
import { ScorecardsService, ScorecardStatus } from 'app/scorecards.service';

import { AddScorecardComponent } from './add-scorecard/add-scorecard.component';


@Component({
   selector: 'app-profile-scorecards',
   templateUrl: './profile-scorecards.component.html',
   styleUrls: ['./profile-scorecards.component.scss']
})

export class ProfileScorecardsComponent implements OnInit {
   username: string = "something";
   scorecards: Scorecard[];
   page: number = 1;

   constructor(private route: ActivatedRoute, private scorecardsService: ScorecardsService) { }



   getScorecards(): void {
      this.scorecardsService.getScorecards(this.route.snapshot.params['username'])
         .then(scorecards => this.scorecards = scorecards);
   }

   onScorecardAdded(model: Scorecard) {
      this.scorecards.push(model);
   }

   canShowManageControl(): boolean {
		//let currentLoginInfo = {};
		return true;
   }

   canShowAddControl(): boolean {
      return true;
   }

	onApproveScorecard(id: string) {
		this.scorecardsService.update(id, ScorecardStatus.Approved);
		return false;
	}

	onDisaproveScorecard(id: string) {
		this.scorecardsService.update(id, ScorecardStatus.Rejected);
		return false;
   }


   ngOnInit() {
      this.username = this.route.snapshot.params['username'];
      this.getScorecards();
   }

   onScroll() {
      this.scorecardsService.getScorecards(this.route.snapshot.params['username'], this.page++)
         .then(scorecards => this.scorecards.push.apply(this.scorecards, scorecards));

   }
}
