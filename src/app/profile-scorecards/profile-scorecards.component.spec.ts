﻿import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileScorecardsComponent } from './profile-scorecards.component';

@Component({
    template: ''
})
class MockProfileScorecardsComponent { }

@NgModule({
    declarations: [MockProfileScorecardsComponent],
    exports: [MockProfileScorecardsComponent]
})
class MockModule { }

describe('ProfileScorecardsComponent', () => {
  let component: ProfileScorecardsComponent;
  let fixture: ComponentFixture<ProfileScorecardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ProfileScorecardsComponent],
        imports: [MockModule, RouterTestingModule.withRoutes([
            { path: '', component: MockProfileScorecardsComponent },
            { path: 'profiles/:username/scorecards', component: MockProfileScorecardsComponent },
        ])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileScorecardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
