﻿import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Scorecard } from 'app/scorecard';
import { ScorecardsService } from 'app/scorecards.service';

@Component({
   selector: 'app-add-scorecard',
   templateUrl: './add-scorecard.component.html',
   styleUrls: ['./add-scorecard.component.scss']
})

export class AddScorecardComponent {

   constructor(private route: ActivatedRoute, private scorecardsService: ScorecardsService) { };


   @Output() onScorecardAdded = new EventEmitter<Scorecard>();

	private newGuid() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
	});
	
}

	model = new Scorecard(this.newGuid() , "Me", this.route.snapshot.params['username'], Date.now().toString(), "", "");

   submitted = false;
   loading = false;
   showForm = false;


   onSubmit() {
      this.submitted = true;
      this.loading = true;

      //if (this.loginService.getCurrentLoginInfo().isLoggedIn) {
         this.model = new Scorecard(this.model.id + 1, this.route.snapshot.params['username'],"Me",  Date.now().toString(), this.model.message, "");
         this.onScorecardAdded.emit(this.model);

         console.log(this.scorecardsService.addScorecard(this.route.snapshot.params['username'], this.model.message));
      //}
      this.loading = false;
   }

   // TODO: Remove this when we're done
   get diagnostic() {
      return JSON.stringify(this.model);
   }

}
