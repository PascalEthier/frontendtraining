﻿export class Scorecard {

   constructor(
      public id: string,
      public ownerUsername: string,
      public fromUsername: string,
      public timestamp: string,
      public message: string,
      public imgUrl: string
   ) { }
}
