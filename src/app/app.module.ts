﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HttpModule } from '@angular/http';

import { FormsModule } from '@angular/forms';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';


import { HeaderComponent } from './header/header.component';
import { AppComponent } from './app.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { ProfileScorecardsComponent } from './profile-scorecards/profile-scorecards.component';
import { AddScorecardComponent } from './profile-scorecards/add-scorecard/add-scorecard.component';
import { LoginService } from './login.service';
import { ScorecardsService } from './scorecards.service';
import { ApproveDisaproveComponent } from './profile-scorecards/approve-disaprove/approve-disaprove.component';

const appRoutes: Routes = [
   { path: '', component: ProfileListComponent },
   { path: 'profiles/:username/scorecards', component: ProfileScorecardsComponent },
];

@NgModule({
   declarations: [
      AppComponent,
      HeaderComponent,
      ProfileListComponent,
      ProfileScorecardsComponent,
      AddScorecardComponent,
      ApproveDisaproveComponent
   ],
   imports: [
      BrowserModule,
      RouterModule.forRoot(appRoutes),
      FormsModule,
      HttpModule,
      InfiniteScrollModule
   ],
   providers: [ScorecardsService],
   bootstrap: [AppComponent],
   schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
