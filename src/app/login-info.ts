﻿export class LoginInfo {
    constructor(
        public isLoggedIn: boolean,
        public username?: string
    ) { }
}
