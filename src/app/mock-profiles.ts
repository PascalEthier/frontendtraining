﻿import { Profile } from './profile';
export const PROFILES: Profile[] = [
    { "name": "Uranus" },
    { "name": "HotToddy" },
    { "name": "Jackd" },
    { "name": "FeelX" }
];