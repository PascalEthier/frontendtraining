﻿import { Injectable } from '@angular/core';
import { Scorecard } from './scorecard';
import { SCORECARDS } from './mock-scorecards';
import { environment } from '../environments/environment';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

const PAGE_SIZE = environment.pageSize || 10;
const API_URL = environment.apiUrl;
const IMAGES: [string] = ["http://dummyimage.com/100x100.jpg/dddddd/000000",
	"http://dummyimage.com/100x100.jpg/5fa2dd/ffffff",
	"http://dummyimage.com/100x100.jpg/dddddd/000000",
	"http://dummyimage.com/100x100.jpg/cc0000/ffffff"]

export const enum ScorecardStatus {
	Approved = 1,
   Rejected = 2
}

@Injectable()
export class ScorecardsService {

	constructor(private http: Http) { }

	getScorecards(username: string, page?: number): Promise<Scorecard[]> {
		if (page == null) {
			page = 1;
		}
		//return this.http
		//	.get(API_URL + "/scorecards/" + username + "?pg=" + page.toString() + "&pgSize=10", { withCredentials: true })
		//	.toPromise()
		//	.then(response => {
		//		return response.json()["scorecards"].map(scorecard => {
		//			scorecard.imgUrl = scorecard.imgUrl || IMAGES[scorecard.fromUsername.charCodeAt(0) % 4];
		//			scorecard.timestamp = scorecard.timestamp || scorecard.createDate;
		//			return scorecard;
		//		});
		//	})
		//	.catch(this.handleError);
		return new Promise<Scorecard[]>((resolve, reject) => {
			resolve(SCORECARDS);
		});
   }

   addScorecard(ownerUsername: string, message: string): Promise<any> {
		return new Promise<any>((resolve, reject) => { resolve(); });
 //     return this.http
 //        .post(API_URL + "/scorecard",
 //           {
 //              "ownerUsername": ownerUsername,
 //              "message": message
 //           },
 //           {
 //              withCredentials: true
 //           }
 //        )
 //        .toPromise()
 //        .then(response => {
 //           console.log(response.json());
 //           return response.json()[""].map(scorecard => {
 //              scorecard.imgUrl = scorecard.imgUrl || IMAGES[scorecard.fromUsername.charCodeAt(0) % 4];
 //              scorecard.timestamp = scorecard.timestamp || scorecard.createDate;
 //              return scorecard;
 //           });
 //        })
 //        .catch(this.handleError);
	}

	update(scorecardId: string, status: ScorecardStatus): Promise<any>{
		return new Promise<any>((resolve, reject) => { resolve();});
	//	return this.http
	//		.patch(API_URL + "/scorecard/" + scorecardId.toString() + "?status=" + status.toString(),null,
	//		{
	//			withCredentials: true
	//		}
	//		)
	//		.toPromise()
	//		.then(response => {
	//			   return true;
	//		 })
	//		.catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error); // for demo purposes only
		return Promise.reject(error.message || error);
	}

}
