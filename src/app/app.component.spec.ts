﻿import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { Component, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HeaderComponent } from './header/header.component';

import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

@Component({
    template: ''
})
class MockProfileScorecardsComponent { }

@NgModule({
    declarations: [MockProfileScorecardsComponent, HeaderComponent],
    exports: [MockProfileScorecardsComponent, HeaderComponent ]
})
class MockModule { }

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
        ],
      imports: [MockModule, RouterTestingModule.withRoutes([
          { path: '', component: MockProfileScorecardsComponent },
          { path: 'profiles/:username/scorecards', component: MockProfileScorecardsComponent },
      ])]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
});
