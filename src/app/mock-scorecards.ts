﻿import { Scorecard } from './scorecard';

export const SCORECARDS: Scorecard[] = [
	{
		"id": "1",
		"ownerUsername": "glebarr0",
		"fromUsername": "mtibols0",
		"timestamp": "2017-05-14 21:49:34",
		"message": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "2",
		"ownerUsername": "hhirtz1",
		"fromUsername": "tborrel1",
		"timestamp": "2016-10-23 21:24:35",
		"message": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff"
	},
	{
		"id": "3",
		"ownerUsername": "lbeazley2",
		"fromUsername": "cberndtssen2",
		"timestamp": "2016-11-30 02:30:59",
		"message": "Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "4",
		"ownerUsername": "phembling3",
		"fromUsername": "pwheaton3",
		"timestamp": "2017-03-02 12:29:30",
		"message": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/cc0000/ffffff"
	},
	{
		"id": "5",
		"ownerUsername": "srobilliard4",
		"fromUsername": "ulaverty4",
		"timestamp": "2016-06-29 00:24:32",
		"message": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "6",
		"ownerUsername": "pnorewood5",
		"fromUsername": "lelwell5",
		"timestamp": "2016-08-02 23:09:58",
		"message": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/ff4444/ffffff"
	},
	{
		"id": "7",
		"ownerUsername": "msells6",
		"fromUsername": "oszymaniak6",
		"timestamp": "2017-02-05 21:25:49",
		"message": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/cc0000/ffffff"
	},
	{
		"id": "8",
		"ownerUsername": "mtanzer7",
		"fromUsername": "rfrangello7",
		"timestamp": "2017-01-01 19:53:00",
		"message": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/ff4444/ffffff"
	},
	{
		"id": "9",
		"ownerUsername": "rgrigor8",
		"fromUsername": "edyka8",
		"timestamp": "2017-04-12 04:33:03",
		"message": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff"
	},
	{
		"id": "10",
		"ownerUsername": "ibarrim9",
		"fromUsername": "zaucutt9",
		"timestamp": "2016-12-18 08:26:16",
		"message": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "11",
		"ownerUsername": "xcolta",
		"fromUsername": "ocostigana",
		"timestamp": "2017-01-26 21:10:05",
		"message": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/cc0000/ffffff"
	},
	{
		"id": "12",
		"ownerUsername": "aschulzeb",
		"fromUsername": "bsichb",
		"timestamp": "2016-12-08 08:21:28",
		"message": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "13",
		"ownerUsername": "jsimondc",
		"fromUsername": "tciepluchc",
		"timestamp": "2017-05-06 23:11:57",
		"message": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "14",
		"ownerUsername": "csemeredd",
		"fromUsername": "bjennensd",
		"timestamp": "2016-09-06 11:55:19",
		"message": "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/cc0000/ffffff"
	},
	{
		"id": "15",
		"ownerUsername": "kfinckee",
		"fromUsername": "tkoline",
		"timestamp": "2016-11-02 04:29:32",
		"message": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "16",
		"ownerUsername": "smahonf",
		"fromUsername": "mfritschmannf",
		"timestamp": "2016-08-18 01:43:13",
		"message": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/cc0000/ffffff"
	},
	{
		"id": "17",
		"ownerUsername": "nbonaviag",
		"fromUsername": "agodsalg",
		"timestamp": "2016-09-08 22:42:01",
		"message": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	},
	{
		"id": "18",
		"ownerUsername": "sstannahh",
		"fromUsername": "qdrinkelh",
		"timestamp": "2016-07-14 18:05:28",
		"message": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff"
	}, {
		"id": "19",
		"ownerUsername": "gkingabyi",
		"fromUsername": "bstaintonskinni",
		"timestamp": "2016-08-20 11:37:46",
		"message": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff"
	}, {
		"id": "20",
		"ownerUsername": "skembryj",
		"fromUsername": "dvolkj",
		"timestamp": "2017-02-14 19:16:28",
		"message": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
		"imgUrl": "http://dummyimage.com/100x100.jpg/dddddd/000000"
	}
];